= Docker Image for Antora
:badges:
:url-antora: https://antora.org
:url-asciidoctor: https://asciidoctor.org
:url-license: https://www.mozilla.org/en-US/MPL/2.0/
:url-docker: https://www.docker.com/
:url-docker-hub: https://hub.docker.com/r/antora/antora
:url-podman: https://podman.io/

This repository hosts the official Docker image for Antora.
The Docker image is used to run Antora inside a container using the Docker client or Podman (or any client that adheres to the OCI standard).

ifdef::badges[]
image:https://shields.io/docker/pulls/antora/antora[pulls,link={url-docker-hub}]
image:https://shields.io/docker/v/antora/antora/latest[latest,link={url-docker-hub}/tags?name=latest]
image:https://shields.io/docker/v/antora/antora/testing[testing,link={url-docker-hub}/tags?name=testing]
endif::[]

== What is Antora?

{url-antora}[Antora] is a modular static site generator designed for creating documentation sites from AsciiDoc documents.
Antora's site generator aggregates documents from versioned content repositories, processes them using {url-asciidoctor}[Asciidoctor], and publishes them together as a documentation site.

== Versioning Policy

There's exactly one version of the image for each released version of Antora.
That relationship includes Antora prereleases (e.g., 2.3.0-beta.1).

The greatest stable release is published under the Docker tag "latest".
The greatest prerelease is published under the Docker tag "testing".
These tags correspond to the tags applied to the packages in the npm repository.

[#use-image]
== How to Use this Image

The name of the image is `antora/antora`.
This image is published to {url-docker-hub}[Docker Hub].
The purpose of the image is to execute the `antora` command inside a container (as configured by the image).
Currently, this image only provides a linux/amd64 container.

If you want to help improve this image, you should learn how to <<build-image,build and install it locally>>.

[#run-image]
=== Run the Image Directly

This image is primarily designed to be used as a command in a box.
You can use this image as a replacement for the `antora` command to execute a playbook.
(The arguments that follow the name of the image are assumed to either be arguments to the `antora` command or a local command).
The benefit of using this image is that you don't have to install Antora itself.

To demonstrate how to use this image, we'll be using the https://gitlab.com/antora/demo/docs-site[Antora demo site].
We'll provide instructions for using the {url-docker}[Docker client] or {url-podman}[Podman].

Start by cloning the playbook repository for the demo site, then switch to the newly created folder:

 $ git clone https://gitlab.com/antora/demo/docs-site.git
   cd docs-site

Next, execute the `docker run` command to start a container process from this image, which implicitly runs the `antora` command inside the container:

 $ docker run -v $PWD:/antora --rm -t antora/antora --stacktrace antora-playbook.yml

The `-t` flag allocates a pseudo-TTY, which is required if you want to see the progress bars for git operations.

Alternately, you can execute the `podman run` command:

 $ podman run -v $PWD:/antora --rm -t antora/antora --stacktrace antora-playbook.yml

The advantage of Podman is that it is more secure.
It runs in user space and does not rely on a daemon.

If you're running a Linux distribution that has SELinux enabled, like Fedora, you'll need to add the `:Z` (or `:z`) modifier to the volume mount.
You'll also want to add the `-u $(id -u):$(id -g)` option to instruct Docker to run the entrypoint command as the current user/group.
Otherwise, files will be written as root and thus hard to delete.
Here's the command you'll use:

 $ docker run -u $(id -u):$(id -g) -v $PWD:/antora:Z --rm -t antora/antora --stacktrace antora-playbook.yml

When using Podman, the `-u` flag is not required since the container is already run in user space.

 $ podman run -v $PWD:/antora:Z --rm -t antora/antora --stacktrace antora-playbook.yml

Although tempting, the `--privileged` flag is not needed.
To learn more about using volume mounts with SELinux, see the blog post http://www.projectatomic.io/blog/2015/06/using-volumes-with-docker-can-cause-problems-with-selinux/[Using Volumes with Docker can Cause Problems with SELinux].

[WARNING]
====
If your uid is not 1000, you may encounter the following error:

 error: EACCES: permission denied, mkdir '/.cache'

This happens because the default cache dir resolves relative to the user's home directory and the home directory of the Docker user is `/` (hence the path [.path]_/.cache_).

You can fix this problem by setting the cache dir relative to the playbook when running Antora:

 $ docker run -u $(id -u):$(id -g) -v $PWD:/antora:Z --rm -t \
   antora/antora --cache-dir=./.cache --stacktrace antora-playbook.yml
====

If you want to shell into the container, use the following command:

 $ docker run -v $PWD:/antora:Z --rm -it antora/antora sh

This command allows you to run the `antora` command from a prompt inside the running container, but will still generate files to the local filesystem.
The reason this works is because, if the first argument following the image name is a local command, the container will execute the specified command instead of `antora`.

=== Align with local paths

If you use the volume mapping `$PWD:/antora:Z`, you may notice that local paths reported by Antora don’t map back to your system.
That's because, as far as Antora is concerned, [.path]_/antora_ is the current working directory.
To remedy this problem, you need to map your current working directory into the container, then switch to it before running Antora.
To do so, use this volume mount instead:

 -v $PWD:$PWD:Z -w $PWD

Notice the addition of the `-w` option.
This option tells Antora to switch from [.path]_/antora_ to the directory you have mapped.
Now, when Antora reports local paths, they will match paths on your system.

[#git-client]
=== Use the git client

Although this image does not include `git`, it does provide access to the CLI for the git client used by Antora (isomorphic-git).
The name of this CLI is `isogit`.
You can use it to clone a repository as follows:

 $ mkdir /tmp/docs-site
   cd /tmp/docs-site
   isogit clone --url=https://gitlab.com/antora/docs.antora.org.git

You can trim that clone down to a single commit by adding additional flags:

   isogit clone --url=https://gitlab.com/antora/docs.antora.org.git --singleBranch --noTags --depth=1

Note that the `isogit clone` command does not create a directory for the repository clone like the `git clone` command.
Therefore, you have to create the repository first, switch to it, then run the `clone` command.

[#gitlab-ci-image]
=== Use the image in GitLab CI

Thanks to the custom entrypoint script ([.path]_docker-entrypoint.sh_), this image will work on GitLab CI without having to specify the entrypoint.
Simply reference the image name at the top of the [.path]_.gitlab-ci.yml_ file, as shown here:

[source,yaml]
----
image:
  name: antora/antora
----

By using this image, you can invoke the `antora` command from a script clause in [.path]_.gitlab-ci.yml_ as follows:

[source,yaml]
----
  script:
  - antora antora-playbook.yml
----

[#extend-image]
=== Use as a Base Image

You can use this image as a base to create your own image.

. Create a custom Dockerfile file named [.path]_Dockerfile.custom_
. Populate that file with the following contents:
+
.Dockerfile.custom
[source,docker]
----
FROM antora/antora

RUN yarn global add asciidoctor-kroki <1>
----
<1> Adds a custom extension to the base image.

. Build the image using the following command:

 $ docker build --pull -t local/antora:custom -f Dockerfile.custom .

Once the build is finished, you can use the image name `local/antora:custom` to run the container.

 $ docker run --rm -t local/antora:custom version

To see a list of your images, run the following command:

 $ docker images

[#add-kroki]
=== Add support for Kroki

The example given for <<extend-image,how to extend this image>> demonstrated adding the Asciidoctor Kroki package to enable support for diagrams generated by https://github.com/yuzutech/kroki[Kroki] in Antora.
To learn how to activate Kroki when using Antora, refer to the https://github.com/asciidoctor/asciidoctor-kroki/blob/master/README.md#antora-integration[Asciidoctor Kroki Antora integration] section.

To use Kroki offline, you can use the https://github.com/yuzutech/kroki/blob/main/README.adoc#companion-containers[docker-compose.yml] file provided by the Kroki project to start Kroki and its companion containers locally.
When doing so, you must set the `kroki-server-url` attribute in the Antora playbook.

[#asciidoctor-pdf]
=== Generate PDFs

The `Dockerfile` below shows how to <<extend-image,extend it>> to building PDFs with support for STEM expressions using `asciidoctor-mathematical`, diagrams generated by <<kroki,Kroki>> and syntax highlighting with `rouge`:

.Dockerfile.custom
[source,docker]
----
FROM antora/antora

RUN apk --no-cache add ruby-bundler build-base bison flex libffi-dev libxml2-dev pango-dev glib-dev gdk-pixbuf-dev cmake python3 ruby-dev graphviz texlive texmf-dist-latexextra

# Add support for generating PDFs
RUN yarn global add --ignore-optional --silent @antora/pdf-extension

RUN echo -e 'source "https://rubygems.org"\n\
\n\
gem "asciidoctor-kroki"\n\
gem "asciidoctor-pdf"\n\
gem "rouge"\n\
gem "asciidoctor-mathematical"\n\
gem "asciimath"\n'\
>> Gemfile && \
bundle
----

Add these lines to your `antora-playbook.yml`:
.antora-playbook.yml
[source,yml]
----
antora:
  extensions:
  - '@antora/pdf-extension'
----

Define your `asciidoctor-pdf` settings in `antora-assembler.yml` like this:

.antora-assembler.yml
[source,yml]
----
#root_level: 0
build:
    command: asciidoctor-pdf -r asciidoctor-kroki --attribute antora-assembler=true --trace --sourcemap
    keep_aggregate_source: true
section_merge_strategy: fuse
icons: font
asciidoc:
  attributes:
    allow-uri-read: ''
    kroki-fetch-diagram: ~
    source-highlighter: rouge
    lang:
    sectnums: true
    sectnumlevels: 5
    toc: ''
    toclevels: 5
    # German translation, courtesy of Florian Wilhelm
    # Asciidoctor doesn't yet support loading a language file so that this is
    # automatic. That's planned as part of asciidoctor/asciidoctor#1129.
    appendix-caption: Anhang
    appendix-refsig: {appendix-caption}
    caution-caption: Achtung
    chapter-signifier: Kapitel
    chapter-refsig: {chapter-signifier}
    example-caption: Beispiel
    figure-caption: Abbildung
    important-caption: Wichtig
    last-update-label: Zuletzt aktualisiert
    listing-caption: Listing
    manname-title: Bezeichnung
    note-caption: Anmerkung
    part-signifier: Teil
    part-refsig: {part-signifier}
    preface-title: Vorwort
    section-refsig: Abschnitt
    table-caption: Tabelle
    tip-caption: Hinweis
    toc-title: Inhaltsverzeichnis
    untitled-label: Ohne Titel
    version-label: Version
    warning-caption: Warnung
----

[#link-git-credentials]
=== Link Credentials for git from Host Machine

Antora uses a <<git-client,JavaScript-based git client>> rather than the native git client.
In order for this client to access private repositories, you must supply it with authentication credentials.
This section describes how to add support for the netrc credential helper to pass credentials from the host machine to the container.

First, install git and the netrc git credential helper in the Antora Docker image:

.Dockerfile.custom
[source,docker]
----
FROM antora/antora

RUN apk --no-cache add git

# <1>
RUN curl \
  -o /usr/bin/git-credential-netrc \
  https://raw.githubusercontent.com/git/git/master/contrib/credential/netrc/git-credential-netrc.perl \
  && chmod +x /usr/bin/git-credential-netrc \
  && ls -al /usr/bin/git-credential-netrc

# <2>
RUN git config --system credential.helper '/usr/bin/git-credential-netrc -f /.netrc -v' \
  && git config --system user.name antora \
  && git config --system user.email antora
----
<1> Use curl to install the netrc git credential helper
<2> Set the git netrc credential helper as the default and configure the name and email

Next, add a custom git credential manager to Antora so it uses the native git credential helper.
To do so, download the https://gitlab.com/antora/antora/-/raw/main/docs/modules/playbook/examples/system-git-credential-manager.js[system-git-credential-manager.js] script (as documented in the https://docs.antora.org/antora/latest/playbook/private-repository-auth/#get-credentials-from-git[Get credentials from git] section in the Antora docs) into your playbook project.
Then, add these lines to your [.path]_antora-playbook.yml_ file:

.antora-playbook.yml
[source,yml]
----
git:
  plugins:
    credential_manager: ./system-git-credential-manager.js
----

Finally, use a volume mount `-v $\{HOME}/.netrc:/.netrc` when <<run-image,running the container>> so the credentials from the host machine are available inside the container.

As an alternative, you can pass the credentials directly to Antora using an environment variable.
This strategy is useful <<gitlab-ci-image,when running Antora in CI>>.
You can pass credentials through an environment variable in GitLab CI by using the following variables stanza in your [.path]_.gitlab-ci.yml_ file:

.gitlab-ci.yml
[source,yml]
----
variables:
  GIT_CREDENTIALS: "https://${CI_DEPENDENCY_PROXY_USER}:${CI_DEPENDENCY_PROXY_PASSWORD}@your.gitlab.loc"
----

This example relies on the https://docs.gitlab.com/ee/user/packages/dependency_proxy/[Dependency proxy for container images].
You can use a different set of CI/CD variables that you define in your project settings.

[#build-image]
== How to Build this Image

To build this image locally, use the following command:

 $ docker build --pull -t local/antora .

The build make take a while to complete.
Once it's finished, you can use the image name `local/antora` (i.e., `local/antora:latest`) to run the container.

== Copyright and License

Copyright (C) 2018-present OpenDevise Inc. and the Antora Project.

Use of this software is granted under the terms of the {url-license}[Mozilla Public License Version 2.0] (MPL-2.0).
See link:LICENSE[] to find the full license text.
