FROM antora/antora:3.2.0-alpha.8

RUN apk --no-cache add ruby-bundler build-base bison flex libffi-dev libxml2-dev pango-dev glib-dev gdk-pixbuf-dev cmake python3 ruby-dev graphviz texlive texmf-dist-latexextra ca-certificates git git-lfs tree

# Add support for generating PDFs
RUN yarn global add --ignore-optional --silent @antora/pdf-extension

RUN echo -e 'source "https://rubygems.org"\n\
\n\
gem "asciidoctor-kroki"\n\
gem "asciidoctor-pdf"\n\
gem "rouge"\n\
gem "asciidoctor-mathematical"\n\
gem "asciimath"\n'\
>> Gemfile && \
bundle

# Add support for diagrams in antora web pages
RUN yarn global add @mermaid-js/mermaid-cli asciidoctor-kroki prismjs

# add custom root certificate
COPY *.crt /usr/local/share/ca-certificates/
RUN update-ca-certificates

# install netrc git credential helper
RUN curl \
  -o /usr/bin/git-credential-netrc \
  https://raw.githubusercontent.com/git/git/master/contrib/credential/netrc/git-credential-netrc.perl \
  && chmod +x /usr/bin/git-credential-netrc \
  && ls -al  /usr/bin/git-credential-netrc

# set git netrc credential helper as default set name and email
RUN git config --system credential.helper '/usr/bin/git-credential-netrc -f /.netrc -v' \
    && git config --system user.name antora \
    && git config --system user.email antora
